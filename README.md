# AWS EC2 User with IAM permissions
* [Boto 3 credentials](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html)
* [Environment variables to configure the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html)

Before ansible can authenticate you will nee the following:

* A new User created using IAM who has direct `Permission policies` or is part of a group of which has at least permissions `AmazonEC2ReadOnlyAccess` or better such as, `AmazonEC2FullAccess` or `AdministratorAccess` if sandboxed.
* `Create access key` within new IAM User under `Security credentials`. Specifically in this example `Command Line Interface` 
* Copy the `Access key` and `Secret Access Key` within a YAML file as described below. Make sure the file containing these keys is secure and not within any git repository.
* Run command: `ansible-galaxy collection install amazon.aws` on host being used to run ansible.
* Encrypt your `key.yml` file using ansible-vault as shown below.
* Connect basic ansible playbook to verify authentication before, shown below.

# Create a yml file for use with ansible-vault

* [Encrypting content with Ansible Vault](https://docs.ansible.com/ansible/latest/vault_guide/vault_encrypting_content.html)

After creating a new user in AWS grab CLI type credentials and add both the access key and the secret key to a YAML file. Also add a region such as `us-east-1` or other regions.

```yaml
--- # AWS Credential information
AWS_ACCESS_KEY_ID: ACCESS_KEY_PLACEHOLDER
AWS_SECRET_ACCESS_KEY: SECRET_ACCESS_KEY_PLACEHOLDER
AWS_REGION: AWS_REGION_PLACEHOLDER
```
This will actually look something like the following. Notice there is no `"` nor `'`around the key strings and a space before each:

```yaml
AWS_ACCESS_KEY_ID: AKIAUG3RTF66XU52BWAV
AWS_SECRET_ACCESS_KEY: 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
AWS_REGION: us-east-1
```
After creating the aforementioned YAML file import it with ansible-vault as follows:

```bash
 ansible-vault encrypt keys.yml
```
You can view the encrypted file using `ansible-vault view keys.yml` if you forget any details later. Now attempt to connect using ansible-playbook using ansible-vault.

```bash
 ansible-playbook --ask-vault-pass test-aws-connection.yml
```

A base version for testing your connection could be as follows:

```yaml
---  # connect to aws environment
- hosts: localhost
  vars_files:
    - keys.yml
  tasks:
  - name: get ec2 instance list
    ec2_instance_info:
      aws_access_key: '{{ AWS_ACCESS_KEY_ID }}'
      aws_secret_key: '{{ AWS_SECRET_ACCESS_KEY }}'
      region: '{{ AWS_REGION }}'
    register: output
  - name: display instance list
    debug:
      var: output.instances
```

