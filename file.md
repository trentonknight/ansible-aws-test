
PLAY [localhost] ***************************************************************

TASK [Gathering Facts] *********************************************************
ok: [localhost]

TASK [get ec2 instance list] ***************************************************
ok: [localhost]

TASK [display instance list] ***************************************************
ok: [localhost] => {
    "output.instances": [
        {
            "ami_launch_index": 0,
            "architecture": "x86_64",
            "block_device_mappings": [],
            "boot_mode": "uefi-preferred",
            "capacity_reservation_specification": {
                "capacity_reservation_preference": "open"
            },
            "client_token": "d71b8b87b90a4c3397ac89c87455b119",
            "cpu_options": {
                "core_count": 1,
                "threads_per_core": 1
            },
            "current_instance_boot_mode": "legacy-bios",
            "ebs_optimized": false,
            "ena_support": true,
            "enclave_options": {
                "enabled": false
            },
            "hibernation_options": {
                "configured": false
            },
            "hypervisor": "xen",
            "image_id": "ami-06ca3ca175f37dd66",
            "instance_id": "i-02fe0496c3f6d4709",
            "instance_type": "t2.micro",
            "key_name": "key_alpha",
            "launch_time": "2023-07-18T16:32:54+00:00",
            "maintenance_options": {
                "auto_recovery": "default"
            },
            "metadata_options": {
                "http_endpoint": "enabled",
                "http_protocol_ipv6": "disabled",
                "http_put_response_hop_limit": 2,
                "http_tokens": "required",
                "instance_metadata_tags": "disabled",
                "state": "pending"
            },
            "monitoring": {
                "state": "disabled"
            },
            "network_interfaces": [],
            "placement": {
                "availability_zone": "us-east-1a",
                "group_name": "",
                "tenancy": "default"
            },
            "platform_details": "Linux/UNIX",
            "private_dns_name": "",
            "product_codes": [],
            "public_dns_name": "",
            "root_device_name": "/dev/xvda",
            "root_device_type": "ebs",
            "security_groups": [],
            "state": {
                "code": 48,
                "name": "terminated"
            },
            "state_reason": {
                "code": "Client.UserInitiatedShutdown",
                "message": "Client.UserInitiatedShutdown: User initiated shutdown"
            },
            "state_transition_reason": "User initiated (2023-07-18 16:35:33 GMT)",
            "tags": {
                "Name": "public-compute-instance"
            },
            "usage_operation": "RunInstances",
            "usage_operation_update_time": "2023-07-18T16:32:54+00:00",
            "virtualization_type": "hvm"
        },
        {
            "ami_launch_index": 1,
            "architecture": "x86_64",
            "block_device_mappings": [],
            "boot_mode": "uefi-preferred",
            "capacity_reservation_specification": {
                "capacity_reservation_preference": "open"
            },
            "client_token": "d71b8b87b90a4c3397ac89c87455b119",
            "cpu_options": {
                "core_count": 1,
                "threads_per_core": 1
            },
            "current_instance_boot_mode": "legacy-bios",
            "ebs_optimized": false,
            "ena_support": true,
            "enclave_options": {
                "enabled": false
            },
            "hibernation_options": {
                "configured": false
            },
            "hypervisor": "xen",
            "image_id": "ami-06ca3ca175f37dd66",
            "instance_id": "i-067c8e8e0e7b8145c",
            "instance_type": "t2.micro",
            "key_name": "key_alpha",
            "launch_time": "2023-07-18T16:32:54+00:00",
            "maintenance_options": {
                "auto_recovery": "default"
            },
            "metadata_options": {
                "http_endpoint": "enabled",
                "http_protocol_ipv6": "disabled",
                "http_put_response_hop_limit": 2,
                "http_tokens": "required",
                "instance_metadata_tags": "disabled",
                "state": "pending"
            },
            "monitoring": {
                "state": "disabled"
            },
            "network_interfaces": [],
            "placement": {
                "availability_zone": "us-east-1a",
                "group_name": "",
                "tenancy": "default"
            },
            "platform_details": "Linux/UNIX",
            "private_dns_name": "",
            "product_codes": [],
            "public_dns_name": "",
            "root_device_name": "/dev/xvda",
            "root_device_type": "ebs",
            "security_groups": [],
            "state": {
                "code": 48,
                "name": "terminated"
            },
            "state_reason": {
                "code": "Client.UserInitiatedShutdown",
                "message": "Client.UserInitiatedShutdown: User initiated shutdown"
            },
            "state_transition_reason": "User initiated (2023-07-18 16:35:33 GMT)",
            "tags": {
                "Name": "public-compute-instance"
            },
            "usage_operation": "RunInstances",
            "usage_operation_update_time": "2023-07-18T16:32:54+00:00",
            "virtualization_type": "hvm"
        },
        {
            "ami_launch_index": 0,
            "architecture": "x86_64",
            "block_device_mappings": [],
            "boot_mode": "uefi-preferred",
            "capacity_reservation_specification": {
                "capacity_reservation_preference": "open"
            },
            "client_token": "b75a3bd1c0ab438b916ba365a7ed6b0e",
            "cpu_options": {
                "core_count": 1,
                "threads_per_core": 1
            },
            "current_instance_boot_mode": "legacy-bios",
            "ebs_optimized": false,
            "ena_support": true,
            "enclave_options": {
                "enabled": false
            },
            "hibernation_options": {
                "configured": false
            },
            "hypervisor": "xen",
            "image_id": "ami-06ca3ca175f37dd66",
            "instance_id": "i-0546966ee40f4bfd3",
            "instance_type": "t2.micro",
            "key_name": "key_alpha",
            "launch_time": "2023-07-18T16:13:07+00:00",
            "maintenance_options": {
                "auto_recovery": "default"
            },
            "metadata_options": {
                "http_endpoint": "enabled",
                "http_protocol_ipv6": "disabled",
                "http_put_response_hop_limit": 2,
                "http_tokens": "required",
                "instance_metadata_tags": "disabled",
                "state": "pending"
            },
            "monitoring": {
                "state": "disabled"
            },
            "network_interfaces": [],
            "placement": {
                "availability_zone": "us-east-1a",
                "group_name": "",
                "tenancy": "default"
            },
            "platform_details": "Linux/UNIX",
            "private_dns_name": "",
            "product_codes": [],
            "public_dns_name": "",
            "root_device_name": "/dev/xvda",
            "root_device_type": "ebs",
            "security_groups": [],
            "state": {
                "code": 48,
                "name": "terminated"
            },
            "state_reason": {
                "code": "Client.UserInitiatedShutdown",
                "message": "Client.UserInitiatedShutdown: User initiated shutdown"
            },
            "state_transition_reason": "User initiated (2023-07-18 16:38:10 GMT)",
            "tags": {
                "Name": "public-compute-instance"
            },
            "usage_operation": "RunInstances",
            "usage_operation_update_time": "2023-07-18T16:13:07+00:00",
            "virtualization_type": "hvm"
        },
        {
            "ami_launch_index": 1,
            "architecture": "x86_64",
            "block_device_mappings": [],
            "boot_mode": "uefi-preferred",
            "capacity_reservation_specification": {
                "capacity_reservation_preference": "open"
            },
            "client_token": "b75a3bd1c0ab438b916ba365a7ed6b0e",
            "cpu_options": {
                "core_count": 1,
                "threads_per_core": 1
            },
            "current_instance_boot_mode": "legacy-bios",
            "ebs_optimized": false,
            "ena_support": true,
            "enclave_options": {
                "enabled": false
            },
            "hibernation_options": {
                "configured": false
            },
            "hypervisor": "xen",
            "image_id": "ami-06ca3ca175f37dd66",
            "instance_id": "i-0667cd996933e97e9",
            "instance_type": "t2.micro",
            "key_name": "key_alpha",
            "launch_time": "2023-07-18T16:13:07+00:00",
            "maintenance_options": {
                "auto_recovery": "default"
            },
            "metadata_options": {
                "http_endpoint": "enabled",
                "http_protocol_ipv6": "disabled",
                "http_put_response_hop_limit": 2,
                "http_tokens": "required",
                "instance_metadata_tags": "disabled",
                "state": "pending"
            },
            "monitoring": {
                "state": "disabled"
            },
            "network_interfaces": [],
            "placement": {
                "availability_zone": "us-east-1a",
                "group_name": "",
                "tenancy": "default"
            },
            "platform_details": "Linux/UNIX",
            "private_dns_name": "",
            "product_codes": [],
            "public_dns_name": "",
            "root_device_name": "/dev/xvda",
            "root_device_type": "ebs",
            "security_groups": [],
            "state": {
                "code": 48,
                "name": "terminated"
            },
            "state_reason": {
                "code": "Client.UserInitiatedShutdown",
                "message": "Client.UserInitiatedShutdown: User initiated shutdown"
            },
            "state_transition_reason": "User initiated (2023-07-18 16:38:10 GMT)",
            "tags": {
                "Name": "public-compute-instance"
            },
            "usage_operation": "RunInstances",
            "usage_operation_update_time": "2023-07-18T16:13:07+00:00",
            "virtualization_type": "hvm"
        },
        {
            "ami_launch_index": 0,
            "architecture": "x86_64",
            "block_device_mappings": [],
            "boot_mode": "uefi-preferred",
            "capacity_reservation_specification": {
                "capacity_reservation_preference": "open"
            },
            "client_token": "293f9f0ea7e145bc83ac167e322551fd",
            "cpu_options": {
                "core_count": 1,
                "threads_per_core": 1
            },
            "current_instance_boot_mode": "legacy-bios",
            "ebs_optimized": false,
            "ena_support": true,
            "enclave_options": {
                "enabled": false
            },
            "hibernation_options": {
                "configured": false
            },
            "hypervisor": "xen",
            "image_id": "ami-06ca3ca175f37dd66",
            "instance_id": "i-0080510016cfdb174",
            "instance_type": "t2.micro",
            "launch_time": "2023-07-18T14:07:01+00:00",
            "maintenance_options": {
                "auto_recovery": "default"
            },
            "metadata_options": {
                "http_endpoint": "enabled",
                "http_protocol_ipv6": "disabled",
                "http_put_response_hop_limit": 2,
                "http_tokens": "required",
                "instance_metadata_tags": "disabled",
                "state": "pending"
            },
            "monitoring": {
                "state": "disabled"
            },
            "network_interfaces": [],
            "placement": {
                "availability_zone": "us-east-1a",
                "group_name": "",
                "tenancy": "default"
            },
            "platform_details": "Linux/UNIX",
            "private_dns_name": "",
            "product_codes": [],
            "public_dns_name": "",
            "root_device_name": "/dev/xvda",
            "root_device_type": "ebs",
            "security_groups": [],
            "state": {
                "code": 48,
                "name": "terminated"
            },
            "state_reason": {
                "code": "Client.UserInitiatedShutdown",
                "message": "Client.UserInitiatedShutdown: User initiated shutdown"
            },
            "state_transition_reason": "User initiated (2023-07-18 16:15:05 GMT)",
            "tags": {
                "Name": "public-compute-instance"
            },
            "usage_operation": "RunInstances",
            "usage_operation_update_time": "2023-07-18T14:07:01+00:00",
            "virtualization_type": "hvm"
        },
        {
            "ami_launch_index": 0,
            "architecture": "x86_64",
            "block_device_mappings": [
                {
                    "device_name": "/dev/xvda",
                    "ebs": {
                        "attach_time": "2023-07-18T16:40:14+00:00",
                        "delete_on_termination": true,
                        "status": "attached",
                        "volume_id": "vol-07c8c089ed2640464"
                    }
                }
            ],
            "boot_mode": "uefi-preferred",
            "capacity_reservation_specification": {
                "capacity_reservation_preference": "open"
            },
            "client_token": "aa579ade102d41ca804cd7978f31af34",
            "cpu_options": {
                "core_count": 1,
                "threads_per_core": 1
            },
            "current_instance_boot_mode": "legacy-bios",
            "ebs_optimized": false,
            "ena_support": true,
            "enclave_options": {
                "enabled": false
            },
            "hibernation_options": {
                "configured": false
            },
            "hypervisor": "xen",
            "image_id": "ami-06ca3ca175f37dd66",
            "instance_id": "i-090db2735a45899a2",
            "instance_type": "t2.micro",
            "key_name": "key_alpha",
            "launch_time": "2023-07-18T16:40:14+00:00",
            "maintenance_options": {
                "auto_recovery": "default"
            },
            "metadata_options": {
                "http_endpoint": "enabled",
                "http_protocol_ipv6": "disabled",
                "http_put_response_hop_limit": 2,
                "http_tokens": "required",
                "instance_metadata_tags": "disabled",
                "state": "applied"
            },
            "monitoring": {
                "state": "disabled"
            },
            "network_interfaces": [
                {
                    "association": {
                        "ip_owner_id": "amazon",
                        "public_dns_name": "ec2-3-95-199-197.compute-1.amazonaws.com",
                        "public_ip": "3.95.199.197"
                    },
                    "attachment": {
                        "attach_time": "2023-07-18T16:40:14+00:00",
                        "attachment_id": "eni-attach-01dde1740789304eb",
                        "delete_on_termination": true,
                        "device_index": 0,
                        "network_card_index": 0,
                        "status": "attached"
                    },
                    "description": "",
                    "groups": [
                        {
                            "group_id": "sg-0621dcb0ac4a83fcf",
                            "group_name": "default"
                        }
                    ],
                    "interface_type": "interface",
                    "ipv6_addresses": [],
                    "mac_address": "12:04:6e:ca:a8:d9",
                    "network_interface_id": "eni-09506271219211285",
                    "owner_id": "118879900181",
                    "private_dns_name": "ip-172-31-86-216.ec2.internal",
                    "private_ip_address": "172.31.86.216",
                    "private_ip_addresses": [
                        {
                            "association": {
                                "ip_owner_id": "amazon",
                                "public_dns_name": "ec2-3-95-199-197.compute-1.amazonaws.com",
                                "public_ip": "3.95.199.197"
                            },
                            "primary": true,
                            "private_dns_name": "ip-172-31-86-216.ec2.internal",
                            "private_ip_address": "172.31.86.216"
                        }
                    ],
                    "source_dest_check": true,
                    "status": "in-use",
                    "subnet_id": "subnet-0c3dc1b234ce8058b",
                    "vpc_id": "vpc-097c3fe7f5ad6b3a6"
                }
            ],
            "placement": {
                "availability_zone": "us-east-1a",
                "group_name": "",
                "tenancy": "default"
            },
            "platform_details": "Linux/UNIX",
            "private_dns_name": "ip-172-31-86-216.ec2.internal",
            "private_dns_name_options": {
                "enable_resource_name_dns_a_record": false,
                "enable_resource_name_dns_aaaa_record": false,
                "hostname_type": "ip-name"
            },
            "private_ip_address": "172.31.86.216",
            "product_codes": [],
            "public_dns_name": "ec2-3-95-199-197.compute-1.amazonaws.com",
            "public_ip_address": "3.95.199.197",
            "root_device_name": "/dev/xvda",
            "root_device_type": "ebs",
            "security_groups": [
                {
                    "group_id": "sg-0621dcb0ac4a83fcf",
                    "group_name": "default"
                }
            ],
            "source_dest_check": true,
            "state": {
                "code": 16,
                "name": "running"
            },
            "state_transition_reason": "",
            "subnet_id": "subnet-0c3dc1b234ce8058b",
            "tags": {
                "Name": "public-compute-instance"
            },
            "usage_operation": "RunInstances",
            "usage_operation_update_time": "2023-07-18T16:40:14+00:00",
            "virtualization_type": "hvm",
            "vpc_id": "vpc-097c3fe7f5ad6b3a6"
        },
        {
            "ami_launch_index": 1,
            "architecture": "x86_64",
            "block_device_mappings": [
                {
                    "device_name": "/dev/xvda",
                    "ebs": {
                        "attach_time": "2023-07-18T16:40:14+00:00",
                        "delete_on_termination": true,
                        "status": "attached",
                        "volume_id": "vol-07500a9f9a6b43e48"
                    }
                }
            ],
            "boot_mode": "uefi-preferred",
            "capacity_reservation_specification": {
                "capacity_reservation_preference": "open"
            },
            "client_token": "aa579ade102d41ca804cd7978f31af34",
            "cpu_options": {
                "core_count": 1,
                "threads_per_core": 1
            },
            "current_instance_boot_mode": "legacy-bios",
            "ebs_optimized": false,
            "ena_support": true,
            "enclave_options": {
                "enabled": false
            },
            "hibernation_options": {
                "configured": false
            },
            "hypervisor": "xen",
            "image_id": "ami-06ca3ca175f37dd66",
            "instance_id": "i-0d84e8cfcad8db624",
            "instance_type": "t2.micro",
            "key_name": "key_alpha",
            "launch_time": "2023-07-18T16:40:14+00:00",
            "maintenance_options": {
                "auto_recovery": "default"
            },
            "metadata_options": {
                "http_endpoint": "enabled",
                "http_protocol_ipv6": "disabled",
                "http_put_response_hop_limit": 2,
                "http_tokens": "required",
                "instance_metadata_tags": "disabled",
                "state": "applied"
            },
            "monitoring": {
                "state": "disabled"
            },
            "network_interfaces": [
                {
                    "association": {
                        "ip_owner_id": "amazon",
                        "public_dns_name": "ec2-3-84-218-188.compute-1.amazonaws.com",
                        "public_ip": "3.84.218.188"
                    },
                    "attachment": {
                        "attach_time": "2023-07-18T16:40:14+00:00",
                        "attachment_id": "eni-attach-048f048dcd657a5db",
                        "delete_on_termination": true,
                        "device_index": 0,
                        "network_card_index": 0,
                        "status": "attached"
                    },
                    "description": "",
                    "groups": [
                        {
                            "group_id": "sg-0621dcb0ac4a83fcf",
                            "group_name": "default"
                        }
                    ],
                    "interface_type": "interface",
                    "ipv6_addresses": [],
                    "mac_address": "12:54:0e:4b:87:6f",
                    "network_interface_id": "eni-0ec741bc8ba30278c",
                    "owner_id": "118879900181",
                    "private_dns_name": "ip-172-31-85-137.ec2.internal",
                    "private_ip_address": "172.31.85.137",
                    "private_ip_addresses": [
                        {
                            "association": {
                                "ip_owner_id": "amazon",
                                "public_dns_name": "ec2-3-84-218-188.compute-1.amazonaws.com",
                                "public_ip": "3.84.218.188"
                            },
                            "primary": true,
                            "private_dns_name": "ip-172-31-85-137.ec2.internal",
                            "private_ip_address": "172.31.85.137"
                        }
                    ],
                    "source_dest_check": true,
                    "status": "in-use",
                    "subnet_id": "subnet-0c3dc1b234ce8058b",
                    "vpc_id": "vpc-097c3fe7f5ad6b3a6"
                }
            ],
            "placement": {
                "availability_zone": "us-east-1a",
                "group_name": "",
                "tenancy": "default"
            },
            "platform_details": "Linux/UNIX",
            "private_dns_name": "ip-172-31-85-137.ec2.internal",
            "private_dns_name_options": {
                "enable_resource_name_dns_a_record": false,
                "enable_resource_name_dns_aaaa_record": false,
                "hostname_type": "ip-name"
            },
            "private_ip_address": "172.31.85.137",
            "product_codes": [],
            "public_dns_name": "ec2-3-84-218-188.compute-1.amazonaws.com",
            "public_ip_address": "3.84.218.188",
            "root_device_name": "/dev/xvda",
            "root_device_type": "ebs",
            "security_groups": [
                {
                    "group_id": "sg-0621dcb0ac4a83fcf",
                    "group_name": "default"
                }
            ],
            "source_dest_check": true,
            "state": {
                "code": 16,
                "name": "running"
            },
            "state_transition_reason": "",
            "subnet_id": "subnet-0c3dc1b234ce8058b",
            "tags": {
                "Name": "public-compute-instance"
            },
            "usage_operation": "RunInstances",
            "usage_operation_update_time": "2023-07-18T16:40:14+00:00",
            "virtualization_type": "hvm",
            "vpc_id": "vpc-097c3fe7f5ad6b3a6"
        }
    ]
}

PLAY RECAP *********************************************************************
localhost                  : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

